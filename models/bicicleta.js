/*let Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: '+this.id+' | color: '+ this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}
let a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
let b = new Bicicleta(2, 'blanco', 'urbana', [-34.5912424, -58.3808497]);
Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;*/



class Bicicleta{
    constructor(id, color, modelo, ubicacion){
        this.id = id;
        this.color = color;
        this.modelo = modelo;
        this.ubicacion = ubicacion;
    }
    toString(){
        return 'id: '+this.id+' | color: '+ this.color;
    }
}
    
Bicicleta.todo = [];
Bicicleta.add = function(aBici){
    Bicicleta.todo.push(aBici)
}

Bicicleta.remove = function(aBiciId){
    Bicicleta.findByID(aBiciId);
    for(let i = 0; i< Bicicleta.todo.length; i++){
        if(Bicicleta.todo[i].id == aBiciId){
            Bicicleta.todo.splice(i,1);
            break;
        }
    }
}

Bicicleta.findByID = function(aBiciId){
    let aBici = Bicicleta.todo.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else 
        throw new Error('No existe');
}

let a = new Bicicleta(1, 'rojo', 'urbana', [-25.518152, -54.620061]);
let b = new Bicicleta(2, 'blanco', 'urbana', [-25.519052, -54.620061]);
let c = new Bicicleta(3, 'negro', 'montaña', [-25.519052, -54.619099]);



Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);


module.exports = Bicicleta;

