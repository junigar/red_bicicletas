const Bicicleta = require('../models/bicicleta');
// let Bicicleta = require('../models/bicicleta');


exports.bicicleta_list = function(req, res){
    res.render('bicicletas/', {bicis: Bicicleta.todo});
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.remove(req.body.id);
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function(req, res){
    let bici = Bicicleta.findByID(req.params.id);
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = function(req, res){
    let bici = Bicicleta.findByID(req.body.id);
    bici.modelo = req.body.modelo;
    bici.color = req.body.color;
    let coord = [req.body.lat, req.body.lng];
    bici.ubicacion = coord;
    res.redirect('/bicicletas');
}
