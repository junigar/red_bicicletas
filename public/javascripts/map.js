let mymap = L.map('main_map').setView([-25.518152, -54.620061], 16);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

// L.marker([-25.518152, -54.620061]).addTo(mymap);
// L.marker([-25.519052, -54.620061]).addTo(mymap);
// L.marker([-25.519052, -54.619099]).addTo(mymap);


$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: function(result){
		console.log(result);		
		result.bicicletas.forEach(function(bici){
			L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
		});
	}
})